angular.module( 'sample.home', [
'auth0'
])
.controller( 'HomeCtrl', function HomeController( $scope, auth, $http, $location, store ) {

  $scope.auth = auth;
  console.log(auth);

  $scope.survey = {};
  // $scope.getpincode={};
  $scope.isAnyUnanswered = function() {
    var count = 0;
    angular.forEach($scope.survey, function(student){
        count += 1;
    });
    if (count == 11 && $scope.isPinValid())
      return false;  // Enable submit button
    else
      return true;
  };

  $scope.isPinValid = function() {
    var regexp = /^\d{6}$/;
    if( $scope.pincode == undefined || $scope.pincode === false ) {
        return false;
    }
    return regexp.test($scope.pincode);
  };

  $scope.submitResponse = function() {

    var user_profile = store.get('profile');
    var params = {};

    params.user_id = user_profile.user_id;
    params.name = user_profile.name;
    params.gender = user_profile.gender;
    params.nickname = user_profile.nickname;
    params.email = user_profile.email;
    params.profile_picture = user_profile.picture;

    params.surveyResponse = $scope.survey;
    params.pincode = $scope.pincode;

    console.log(params);
    $http.post('survey/submitresponse',params).
        success(function(data) {
            console.log("posted successfully");
            $location.path('/confirm');
        }).error(function(data) {
            console.error("error in posting");
        });

    // Just call the API as you'd do using $http
    // $http({
    //   url: 'http://localhost:3001/secured/ping',
    //   method: 'GET'
    // }).then(function() {
    //   alert("We got the secured data successfully");
    // }, function(response) {
    //   if (response.status == 0) {
    //     alert("Please download the API seed so that you can call it.");
    //   }
    //   else {
    //     alert(response.data);
    //   }
    // });
  }

  $scope.logout = function() {
    auth.signout();
    store.remove('profile');
    store.remove('token');
    $location.path('/login');
  }

});
