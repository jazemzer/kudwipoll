angular.module( 'sample.confirm', [
'auth0'
])
.controller( 'ConfirmCtrl', function HomeController( $scope, auth, $http, $location, store ) {
	$scope.profile = store.get('profile');

	if($scope.profile != undefined) {
		var surveyid = $scope.profile.user_id.replace("acebook|","");		
		$scope.surveyid = surveyid.replace("oogle-oauth2|","");		
	}
	$scope.logout = function() {
	    auth.signout();
	    store.remove('profile');
	    store.remove('token');
	    $location.path('/login');
	}

});