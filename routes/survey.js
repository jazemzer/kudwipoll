var express = require('express');
var router = express.Router();
var csv = require('express-csv');
var flatten = require('flat');

var collection_name = 'survey';
router.get('/list', function(req, res) {
    var db = req.db;
    var collection = db.get(collection_name);
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

router.get('/results', function (req, res) {
    var db = req.db;
    var collection = db.get(collection_name);
    collection.find({},{},function(e,docs){
        var data = [];
        docs.forEach(function (item, index, array) {
            delete item._id;
            data.push(flatten(item));
        })
        console.log(data);
        res.csv(data);
    });    
});

router.post('/submitresponse', function(req, res) {

    // Set our internal DB variable
    var db = req.db;

    // Set our collection
    var collection = db.get(collection_name);

    // Submit to the DB
    collection.insert(req.body, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // And forward to success page
            res.send("200").end();
        }
    });
});

module.exports = router;
