var express = require('express');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


var collection_name = 'usercollection';
router.get('/list', function(req, res) {
    var db = req.db;
    var collection = db.get(collection_name);
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

/* POST to Add User Service */
router.post('/adduser', function(req, res) {

    // Set our internal DB variable
    var db = req.db;

    // Set our collection
    var collection = db.get('usercollection');

    // Submit to the DB
    collection.insert(req.body, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // And forward to success page
            res.send("200").end();
        }
    });
});

module.exports = router;
